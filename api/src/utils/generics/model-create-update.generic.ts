import {
  NoteCreateInput,
  UserCreateInput,
  NoteUpdateInput,
  UserUpdateInput,
} from '../../prisma/prisma-client';

export type ModelCreateInput = NoteCreateInput | UserCreateInput;

export type ModelUpdateInput = NoteUpdateInput | UserUpdateInput;
