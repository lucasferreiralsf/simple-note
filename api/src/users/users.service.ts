import { Injectable } from '@nestjs/common';
import { User } from '../prisma/prisma-client';
import * as bcrypt from 'bcrypt';
import { GenericService } from '../utils/generics/generic-service.generic';
import {
  UsersWithTagsNotesPassword,
  UsersWithTagsAndNotes,
} from './users.fragment';
import { UserCreateDto } from './dto/user-create.dto';
import { UserUpdateDto } from './dto/user-update.dto';
import { removeElementObject } from '../utils/helpers/remove-element-object';
import { ConfigService } from '../config/config.service';

@Injectable()
export class UsersService extends GenericService<User> {
  constructor(private readonly configService: ConfigService) {
    super();
  }

  async storeUser(user: UserCreateDto) {
    if (user.password) {
      user.password = await bcrypt.hash(user.password, 12);
    }
    const userReduced = removeElementObject(user, [
      'tags',
      'notes',
    ]);
    const userCreated = await this.create(
      'createUser',
      {
        tags: { connect: user.tags },
        notes: { connect: user.notes },
        ...userReduced.objectReduced,
      },
      UsersWithTagsAndNotes,
    );
    if (userCreated.password || userCreated.emailToken) {
      userCreated.password = undefined;
      userCreated.emailToken = undefined;
    }
    return userCreated;
  }

  async updateUser(
    field: { email: string } | { id: string },
    user: UserUpdateDto,
  ) {
    const userReduced = removeElementObject(user, [
      'tags',
      'notes',
    ]);
    const userUpdated = await this.update(
      'updateUser',
      field,
      {
        tags: { connect: user.tags },
        notes: { connect: user.notes },
        ...userReduced.objectReduced,
      },
      UsersWithTagsAndNotes,
    );
    userUpdated.emailToken = undefined;
    userUpdated.password = undefined;
    return userUpdated;
  }

  async findBy(
    field: { email: string },
    withPassword: boolean = false,
  ) {
    return await this.fetchBy(
      'user',
      field,
      withPassword
        ? UsersWithTagsNotesPassword
        : UsersWithTagsAndNotes,
    );
  }

  async getAll(currentPage: string, perPage: string) {
    return await this.fetchAll(
      'users',
      currentPage,
      perPage,
      UsersWithTagsAndNotes,
    );
  }
}
