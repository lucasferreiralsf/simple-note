import { Injectable } from '@nestjs/common';
import { Prisma } from '../prisma/prisma-client';
import { ConfigService } from '../config/config.service';
import * as path from 'path';

const configService = new ConfigService(
  path.resolve('environments', `${process.env.NODE_ENV || 'dev'}.env`),
);
export const prisma: Prisma = new Prisma({
  endpoint: `https://prisma.becoder.com.br/simplenote/${
    process.env.NODE_ENV || 'dev'
  }`,
  secret: configService.get('PRISMA_SECRET'),
  debug: process.env.NODE_ENV === 'prod' ? false : true,
});
