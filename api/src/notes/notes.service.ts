import { Injectable } from '@nestjs/common';
import { GenericService } from '../utils/generics/generic-service.generic';
import { Note } from '../prisma/prisma-client';
import { ConfigService } from '../config/config.service';
import { NoteCreateDto } from './dto/notes-create';
import { NoteWithTagsAndUsers } from './notes.fragment';
import { removeElementObject } from '../utils/helpers/remove-element-object';
import { NoteUpdateDto } from './dto/notes-update';

@Injectable()
export class NotesService extends GenericService<Note> {
  constructor(private readonly configService: ConfigService) {
    super();
  }

  async storeNote(note: NoteCreateDto, userId: string) {
    const noteReduced = removeElementObject(note, ['tags', 'user']);
    const noteCreated = await this.create(
      'createNote',
      {
        tags: { connect: note.tags },
        user: { connect: { id: userId } },
        content: `{
          "document": {
            "nodes": [
              {
                "object": "block",
                "type": "paragraph",
                "nodes": [
                  {
                    "object": "text",
                    "text": ""
                  }
                ]
              }
            ]
          }
        }`,
      },
      NoteWithTagsAndUsers,
    );

    return noteCreated;
  }

  async updateNote(field: { id: string }, note: NoteUpdateDto, userId: string) {
    const noteReduced = removeElementObject(note, ['tags']);
    const noteUpdated = await this.update(
      'updateNote',
      field,
      {
        tags: { connect: note.tags },
        ...noteReduced.objectReduced,
      },
      NoteWithTagsAndUsers,
    );

    return noteUpdated;
  }

  async deleteNote(field: { id: string }, userId: string) {
    const data = {
      where: { id: userId },
      data: {
        notes: {
          delete: { id: field.id },
        },
      },
    };
    const noteDeleted = await this.delete('updateUser', data);

    return noteDeleted;
  }

  async findBy(field: { id: string }, userId: string) {
    return await this.fetchBy('note', field, NoteWithTagsAndUsers);
  }

  async getAll(currentPage: string, perPage: string, userId: string) {
    return await this.fetchAll(
      'notes',
      currentPage,
      perPage,
      { user: { id: userId } },
      NoteWithTagsAndUsers,
    );
  }
}
